class SlotOne:
    def __init__(self, label='', oncooldown=False):
        self.sid = 1
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown


class SlotTwo:
    def __init__(self, label='', oncooldown=False):
        self.sid = 2
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown


class SlotThree:
    def __init__(self, label='', oncooldown=False):
        self.sid = 3
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown


class SlotFour:
    def __init__(self, label='', oncooldown=False):
        self.sid = 4
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown


class SlotFive:
    def __init__(self, label='', oncooldown=False):
        self.sid = 5
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown


class SlotSix:
    def __init__(self, label='', oncooldown=False):
        self.sid = 6
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown


class SlotSeven:
    def __init__(self, label='', oncooldown=False):
        self.sid = 7
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown


class SlotEight:
    def __init__(self, label='', oncooldown=False):
        self.sid = 8
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown


class SlotNine:
    def __init__(self, label='', oncooldown=False):
        self.sid = 9
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown


class SlotTen:
    def __init__(self, label='', oncooldown=False):
        self.sid = 0
        self.label = label
        self.oncooldown = oncooldown

    def is_it_on_cool_down(self):
        return self.oncooldown
